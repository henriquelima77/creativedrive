﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

[Serializable]
public class TransformationHolder
{
    private TMP_InputField xField, yField, zField;

    public TransformationHolder(TMP_InputField xField, TMP_InputField yField, TMP_InputField zField)
    {
        this.xField = xField;
        this.yField = yField;
        this.zField = zField;
    }

    public void ChangeValue(string transformType)
    {
        if (TransformController.Instance.transformHandle == null || TransformController.Instance.transformHandle.SelectedObject == null)
            return;
        
           
        Transform desiredTransform = TransformController.Instance.transformHandle.SelectedObject.transform;

        if (desiredTransform == null)
            return;
        

        Decimal xF = 0;
        Decimal.TryParse(xField.text, NumberStyles.Number, CultureInfo.InvariantCulture, out xF);

        Decimal yF = 0;
        Decimal.TryParse(yField.text, NumberStyles.Number, CultureInfo.InvariantCulture, out yF);

        Decimal zF = 0;
        Decimal.TryParse(zField.text, NumberStyles.Number, CultureInfo.InvariantCulture, out zF);


        float x = string.IsNullOrEmpty(xField.text) == true ? 0f : (float)xF;
        float y = string.IsNullOrEmpty(yField.text) == true ? 0f : (float)yF;
        float z = string.IsNullOrEmpty(zField.text) == true ? 0f : (float)zF;

        switch (transformType)
        {
            case "Position":
                TransformController.Instance.transformHandle.SelectedObject.transform.position = new Vector3(x, y, z);
            break;

            case "Rotation":
                TransformController.Instance.transformHandle.SelectedObject.transform.eulerAngles = new Vector3(x, y, z);
            break;

            case "Scale":
                TransformController.Instance.transformHandle.SelectedObject.transform.localScale = new Vector3(x, y, z);
            break;
        }
    }

    public void GetValues(Vector3 mainVector)
    {
        xField.text = mainVector.x.ToString(CultureInfo.InvariantCulture);
        yField.text = mainVector.y.ToString(CultureInfo.InvariantCulture);
        zField.text = mainVector.z.ToString(CultureInfo.InvariantCulture);
    }
    
    public void ResetValues()
    {
        xField.text = "0";
        yField.text = "0";
        zField.text = "0";
    }
}
