﻿using UnityEngine;

public class SceneController : MonoBehaviour
{
    [SerializeField]
    private Transform mainRoot;
    [SerializeField]
    private GameObject loadingPanel;

    [SerializeField]
    private string url = "https://s3-sa-east-1.amazonaws.com/static-files-prod/unity3d/models.json";

    private RootObject gameData;

    //modules
    private Hotkeys hotKeys;
    private ObjectsHandler objectsHandler;
    private SaveScene saveScene;
    private LoadScene loadScene;

    private void Start()
    {
        gameData = new RootObject();

        objectsHandler = new ObjectsHandler(mainRoot);
        saveScene = new SaveScene();
        loadScene = new LoadScene(gameData, loadingPanel, objectsHandler);
        hotKeys = new Hotkeys(mainRoot);
    }

    public void Update()
    {
        hotKeys.CheckForHotKeys();
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void RemoveAllObjectsFromScene()
    {
        objectsHandler.DeleteAllSceneObjects();
    }

    public void SaveTheScene()
    {
        saveScene.SaveInFile();
    }

    public void Request()
    {
        StartCoroutine(loadScene.LoadFromRequest(url));
    }

    public void LoadFromFile()
    {
        loadScene.LoadFromFile();
    }
}
