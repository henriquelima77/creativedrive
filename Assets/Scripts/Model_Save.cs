﻿using System.Collections.Generic;
using UnityEngine;

public class Model_Save : MonoBehaviour
{
    public Model Save()
    {
        Model model = new Model();

        model.position = new List<float>();
        model.rotation = new List<float>();
        model.scale = new List<float>();

        model.name = gameObject.name;
        model.position.Add(transform.position.x);
        model.position.Add(transform.position.y);
        model.position.Add(transform.position.z);

        model.rotation.Add(transform.eulerAngles.x);
        model.rotation.Add(transform.eulerAngles.y);
        model.rotation.Add(transform.eulerAngles.z);

        model.scale.Add(transform.localScale.x);
        model.scale.Add(transform.localScale.y);
        model.scale.Add(transform.localScale.z);

        return model;
    }
}
