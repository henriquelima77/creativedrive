﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TransformController : MonoBehaviour
{
    public static TransformController Instance;

    public TransformPanel transformPanel;
    public TransformHandle transformHandle;

    public bool IgnoreRaycast = false;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        transformHandle = new TransformHandle(transformPanel);
    }

    private void Update()
    {
        if(IgnoreRaycast == false)
        {
            transformHandle.GetClickedObject();
        }
    }

    public void EnableRaycast(bool state)
    {
        IgnoreRaycast = state;
    }
}
