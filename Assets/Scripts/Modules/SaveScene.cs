﻿using SFB;
using System.IO;
using UnityEngine;

// https://github.com/gkngkc/UnityStandaloneFileBrowser
// Added this repository to save and load file from file system

public class SaveScene
{
    public void SaveInFile()
    {
        RootObject root = new RootObject();

        Model_Save[] models = GameObject.FindObjectsOfType<Model_Save>();

        for (int i = 0; i < models.Length; i++)
        {
            root.models.Add(models[i].Save());
        }

        string jsonData = JsonUtility.ToJson(root, true);
        ExtensionFilter extensionFilter = new ExtensionFilter("Json file", "json");
        File.WriteAllText(StandaloneFileBrowser.SaveFilePanel("Save File"
            , "", "newScene", new ExtensionFilter[] { extensionFilter }), jsonData);
    }
}
