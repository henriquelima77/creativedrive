﻿using UnityEngine;

public class ObjectsHandler
{
    private Transform mainRoot;

    public ObjectsHandler(Transform mainRoot)
    {
        this.mainRoot = mainRoot;
    }

    public void PopulateScene(RootObject gameData)
    {
        if(gameData.models.Count > 0)
        {
            for (int i = 0; i < gameData.models.Count; i++)
            {
                Vector3 position = new Vector3(gameData.models[i].position[0], gameData.models[i].position[1], gameData.models[i].position[2]);
                Vector3 rotation = new Vector3(gameData.models[i].rotation[0], gameData.models[i].rotation[1], gameData.models[i].rotation[2]);
                Vector3 scale = new Vector3(gameData.models[i].scale[0], gameData.models[i].scale[1], gameData.models[i].scale[2]);
                string name = gameData.models[i].name;
                Instantiator.Instantiate(name, position, rotation, scale, mainRoot);
            }
        }
    }

    public void InstantiateObject(int index)
    {

    }

    public void DeleteAllSceneObjects()
    {
        int childCount = mainRoot.childCount;

        if (childCount == 0)
            return;

        for (int i = 0; i < childCount; i++)
        {
            GameObject.Destroy(mainRoot.GetChild(i).gameObject);
        }
    }
}
