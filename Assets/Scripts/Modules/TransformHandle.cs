﻿using UnityEngine;

[System.Serializable]
public class TransformHandle
{
    public TransformPanel transformPanel;

    private GameObject selectedObject;
    public GameObject SelectedObject
    {
        get => selectedObject;
        set
        {
            if (value == selectedObject)
                return;

            selectedObject = value;

            if(selectedObject != null)
                transformPanel.EnablePanel();
            else
                transformPanel.DisablePanel();
        }
    }

    public TransformHandle(TransformPanel transformPanel)
    {
        this.transformPanel = transformPanel;
    }
    
    public void GetClickedObject()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.transform != null)
                {
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("ClickableObject"))
                    {
                        SelectedObject = hit.transform.gameObject;
                        //transformPanel.EnablePanel();
                    }
                    else
                    {
                        SelectedObject = null;
                        //transformPanel.DisablePanel();
                    }
                }
                else
                {
                    SelectedObject = null;
                    //transformPanel.DisablePanel();
                }
            }
            else
            {
                SelectedObject = null;
                //transformPanel.DisablePanel();
            }
        }
    }
}
