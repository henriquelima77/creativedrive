﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThumbnailsHandle
{
    private Transform mainRoot;
    private List<Button> buttons;

    private int thumbnailsCount;
    private Sprite[] sprites;
    private GameObject[] prefabs;

    public ThumbnailsHandle(Transform mainRoot, List<Button> buttons)
    {
        this.mainRoot = mainRoot;
        this.buttons = new List<Button>();
        this.buttons.AddRange(buttons);

        sprites = Resources.LoadAll<Sprite>("Thumbnails");
        thumbnailsCount = sprites.Length;
        prefabs = Resources.LoadAll<GameObject>("Prefabs");
        for (int i = 0; i < thumbnailsCount; i += 1)
        {
            buttons[i].transform.parent.gameObject.SetActive(true);
            buttons[i].GetComponent<Image>().sprite = sprites[i];
        }
    }

    public void ThumbnailClick(int index)
    {
        GameObject go = Instantiator.Duplicate(prefabs[index], Vector3.zero, Vector3.zero, Vector3.one, mainRoot);
        TransformController.Instance.transformHandle.SelectedObject = go;
    }
}
