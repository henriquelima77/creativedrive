﻿using System.Collections.Generic;
using System;

[Serializable]
public class Model
{
    public string name;
    public List<float> position;
    public List<float> rotation;
    public List<float> scale;
}
