﻿using System.Collections.Generic;
using System;

[Serializable]
public class RootObject
{
    public List<Model> models;
}
