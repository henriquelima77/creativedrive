﻿using SFB;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Text;

public class LoadScene
{
    private GameObject loadingPanel;
    private RootObject gameData;

    //module
    private ObjectsHandler objectsHandler;


    public LoadScene(RootObject gameData, GameObject loadingPanel, ObjectsHandler objectsHandler)
    {
        this.objectsHandler = objectsHandler;
        this.gameData = gameData;
        this.loadingPanel = loadingPanel;
    }

    public void LoadFromFile()
    {
        gameData = new RootObject();
        gameData.models = new List<Model>();

        objectsHandler.DeleteAllSceneObjects();

        ExtensionFilter extensionFilter = new ExtensionFilter("Json file", "json");
        string[] paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", new ExtensionFilter[] { extensionFilter }, false);

        if (paths.Length == 0)
        {
            return;
        }
        else
        {
            string jsonData = File.ReadAllText(paths[0]);
            gameData = JsonUtility.FromJson<RootObject>(jsonData);

            objectsHandler.PopulateScene(gameData);
        }
    }

    public IEnumerator LoadFromRequest(string url)
    {
        loadingPanel.SetActive(true);

        gameData = new RootObject();
        objectsHandler.DeleteAllSceneObjects();

        WWW request = new WWW(url);
        
        yield return request;
        loadingPanel.SetActive(false);

        if(request.error == null || request.error == "")
        {
            string jsonData = Encoding.UTF8.GetString(request.bytes, 3, request.bytes.Length - 3);
            gameData = JsonUtility.FromJson<RootObject>(jsonData);

            objectsHandler.PopulateScene(gameData);
        }
        else
        {
            yield return null;
        }
    }
}
