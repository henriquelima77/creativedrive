﻿using UnityEngine;

public class Hotkeys
{
    private Transform mainRoot;

    public Hotkeys(Transform mainRoot)
    {
        this.mainRoot = mainRoot;
    }

    public void CheckForHotKeys()
    {
        if(Input.GetKey(KeyCode.LeftControl))
        {
            if(Input.GetKeyDown(KeyCode.D))
            {
                GameObject go = TransformController.Instance.transformHandle.SelectedObject;
                if (go != null)
                {
                    TransformController.Instance.transformHandle.SelectedObject = Instantiator.Duplicate(go, Vector3.zero, Vector3.zero, Vector3.one, mainRoot);
                }
            }
        }

        if(!Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Delete))
        {
            GameObject go = TransformController.Instance.transformHandle.SelectedObject;
            if (go != null)
            {
                GameObject.Destroy(go);
                TransformController.Instance.transformHandle.SelectedObject = null;
            }
        }
    }
}
