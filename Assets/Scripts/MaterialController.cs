﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour
{
    public void ChangeColor(int index)
    {
        Color color = Color.white;
        switch(index)
        {
            case 0:
                color = Color.white;
            break;
            case 1:
                color = Color.red;
            break;
            case 2:
                color = Color.blue;
            break;
        }
        GameObject go = TransformController.Instance.transformHandle.SelectedObject;

        MeshRenderer[] meshs = null;
        if(go.GetComponent<MeshRenderer>() == null)
        {
            meshs = go.GetComponentsInChildren<MeshRenderer>();
        }
        else
        {
            meshs = new MeshRenderer[] { go.GetComponent<MeshRenderer>() };
        }

        foreach(MeshRenderer mesh in meshs)
        {
            MaterialPropertyBlock propBlock = new MaterialPropertyBlock();
            mesh.GetPropertyBlock(propBlock);
            propBlock.SetColor("_Color", color);
            mesh.SetPropertyBlock(propBlock);
        }
    }

    public void ChangeTexture(int index)
    {
        Texture texture = null;
        switch(index)
        {
            case 0:
                texture = Resources.Load("Textures/BedRoom0" + index) as Texture;
            break;
            case 1:
                texture = Resources.Load("Textures/BedRoom0" + index) as Texture;
            break;
            case 2:
                texture = Resources.Load("Textures/BedRoom0" + index) as Texture;
            break;
        }

        GameObject go = TransformController.Instance.transformHandle.SelectedObject;
        MeshRenderer[] meshs = null;
        if (go.GetComponent<MeshRenderer>() == null)
        {
            meshs = go.GetComponentsInChildren<MeshRenderer>();
        }
        else
        {
            meshs = new MeshRenderer[] { go.GetComponent<MeshRenderer>() };
        }

        foreach (MeshRenderer mesh in meshs)
        {
            MaterialPropertyBlock propBlock = new MaterialPropertyBlock();
            mesh.GetPropertyBlock(propBlock);
            propBlock.SetTexture("_MainTex", texture);
            mesh.SetPropertyBlock(propBlock);
        }
    }
}
