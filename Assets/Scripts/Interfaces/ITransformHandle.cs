﻿using TMPro;

public interface ITransformHandle
{
    TMP_InputField XInput { get; }
    TMP_InputField YInput { get; }
    TMP_InputField ZInput { get; }

    void ChangeValue();
}

