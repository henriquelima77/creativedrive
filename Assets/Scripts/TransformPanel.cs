﻿using TMPro;
using UnityEngine;

public class TransformPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject panel;

    [SerializeField]
    private TransformationHolder position;
    [SerializeField]
    private TransformationHolder rotation;
    [SerializeField]
    private TransformationHolder scale;

    [SerializeField]
    private TMP_InputField xPositionField, yPositionField, zPositionField;
    [SerializeField]
    private TMP_InputField xRotationField, yRotationField, zRotationField;
    [SerializeField]
    private TMP_InputField xScaleField, yScaleField, zScaleField;

    private void Start()
    {

        position = new TransformationHolder(xPositionField, yPositionField, zPositionField);
        rotation = new TransformationHolder(xRotationField, yRotationField, zRotationField);
        scale = new TransformationHolder(xScaleField, yScaleField, zScaleField);

        DisablePanel();
    }

    public void ApplyTransformationValues()
    {
        if (TransformController.Instance.transformHandle.SelectedObject == null)
            return;

        position.ChangeValue("Position");
        rotation.ChangeValue("Rotation");
        scale.ChangeValue("Scale");
    }

    private void GetTransformationValues()
    {
        Vector3 position = TransformController.Instance.transformHandle.SelectedObject.transform.position;
        Vector3 rotation = TransformController.Instance.transformHandle.SelectedObject.transform.eulerAngles;
        Vector3 scale = TransformController.Instance.transformHandle.SelectedObject.transform.localScale;

        this.position.GetValues(position);
        this.rotation.GetValues(rotation);
        this.scale.GetValues(scale);
    }

    private void ResetTransformationValues()
    {
        position.ResetValues();
        rotation.ResetValues();
        scale.ResetValues();
    }


    public void EnablePanel()
    {
        panel.SetActive(true);

        ResetTransformationValues();
        GetTransformationValues();
    }

    public void DisablePanel()
    {
        ResetTransformationValues();
        panel.SetActive(false);
    }
}
