﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private float lookSpeedH = 2f;
    private float lookSpeedV = 2f;
    private float normalSpeed = 0.2f;
    private float runSpeed = 0.5f;
    private float dragSpeed = 6f;

    private float yaw = 0f;
    private float pitch = 0f;

    private void Update()
    {
        if (Input.GetMouseButton(1))
        {
            yaw += lookSpeedH * Input.GetAxis("Mouse X");
            pitch -= lookSpeedV * Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(pitch, yaw, 0f);
        }

        if (Input.GetMouseButton(2))
        {
            transform.Translate(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * dragSpeed, -Input.GetAxisRaw("Mouse Y") * Time.deltaTime * dragSpeed, 0);
        }

        if(!Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.RightControl))
        {
            CameraTranslation();
        }
            
    }

    private void CameraTranslation()
    {
        float yMovement = 0f;

        if(Input.GetKey(KeyCode.E))
        {
            yMovement = 1f;
        }
        else if(Input.GetKey(KeyCode.Q))
        {
            yMovement = -1f;
        }

        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal"), yMovement, Input.GetAxisRaw("Vertical"));

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            transform.Translate(movement * runSpeed, Space.Self);
        }
        else
        {
            transform.Translate(movement * normalSpeed, Space.Self);
        }
    }
}
