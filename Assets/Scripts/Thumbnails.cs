﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Thumbnails : MonoBehaviour
{
    [SerializeField]
    private List<Button> thumbnails;
    [SerializeField]
    private Transform mainRoot;

    //modules
    private ThumbnailsHandle thumbnailsHandle;

    private void Start()
    {
        thumbnailsHandle = new ThumbnailsHandle(mainRoot, thumbnails);
    }

    public void OnClickEvent(int index)
    {
        thumbnailsHandle.ThumbnailClick(index);
    }
}
