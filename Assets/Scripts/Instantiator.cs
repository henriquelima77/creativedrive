﻿using UnityEngine;

public static class Instantiator
{
    public static GameObject Instantiate(string goName, Vector3 position, Vector3 rotation, Vector3 scale, Transform parent = null)
    {
        GameObject go = Resources.Load("Prefabs/" + goName) as GameObject;
        GameObject gameObject = GameObject.Instantiate(go);
        gameObject.name = go.name;

        gameObject.transform.position = position;
        gameObject.transform.eulerAngles = rotation;
        gameObject.transform.localScale = scale;

        gameObject.transform.SetParent(parent);
        return gameObject;
    }

    public static GameObject Duplicate(GameObject go, Vector3 position, Vector3 rotation, Vector3 scale, Transform parent = null)
    {
        Vector3 pos = Camera.main.transform.position + (Camera.main.transform.forward * 3f);

        GameObject gameObject = GameObject.Instantiate(go, pos, Quaternion.Euler(rotation));
        gameObject.name = go.name;
        gameObject.transform.localScale = scale;
        gameObject.transform.SetParent(parent);

        return gameObject;
    }
}

